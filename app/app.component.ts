import {Component} from '@angular/core';

@Component({
    selector: 'app-root',
    styleUrls: ['app.component.scss'],
    template: `        
        <wco-navigation></wco-navigation>
    `
})
export class AppComponent {
}
