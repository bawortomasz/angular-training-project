
import {Component, Input, OnChanges, OnInit, SimpleChanges} from "@angular/core";
import {NavigationLink} from "../../model/navigation-link.interface";

@Component({
    selector: 'wco-navigation-link',
    styleUrls: ['navigation-link.component.scss'],
    template: `
        <div class="wco-navigation-link">
            <button [ngClass]="{'active': selected}">
                {{navLink.label}}
            </button>
        </div>
    `
})
export class NavigationLinkComponent implements OnChanges, OnInit {
    @Input()
    navLink: NavigationLink;
    @Input()
    selected: boolean;

    constructor() {
    }

    ngOnChanges(changes: SimpleChanges): void {
        console.log('NavigationLinkComponent.ngOnChanges()' + JSON.stringify(changes))
    }

    ngOnInit(): void {
        console.log('NavigationLinkComponent.ngOnInit()')
    }

}