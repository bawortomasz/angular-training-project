import {Component, OnInit} from "@angular/core";
import {NavigationLink} from "../../model/navigation-link.interface";
import {NavigationService} from "../../services/navigation.service";

@Component({
    selector: 'wco-navigation',
    styleUrls: ['navigation.component.scss'],
    template: `
        <div class="wco-navigation">
            <h1>Navigation</h1>
            <ul>
                <li *ngFor="let link of navigationLinks">
                    <wco-navigation-link [selected]="selectedId==link.id" [navLink]="link" (click)="onClick(link)"></wco-navigation-link>
                </li>
            </ul>
            <div class="wco-navigation">
                <div *ngFor="let link of navigationLinks">
                    <div *ngIf="selectedId==link.id">
                        Page: {{link.label}}
                    </div>
                </div>
            </div>
        </div>
    `
})
export class NavigationComponent implements OnInit {
    navigationLinks: NavigationLink[];
    selectedId: number;

    constructor(private navigationService: NavigationService) {
        console.log("NavigationComponent.constructor()")
    }

    ngOnInit(): void {
        this.navigationService
            .getNavigationLinks()
            .subscribe((data: NavigationLink[]) => {
                this.navigationLinks = data;
                this.selectedId = this.navigationLinks[0].id;
            });

    }

    onClick(link: NavigationLink) {
        this.selectedId = link.id;
        console.log("Navigation link clicked: " + JSON.stringify(link))
    }
}