import {Injectable} from "@angular/core";
import {Http, Response} from "@angular/http";

import {NavigationLink} from "../model/navigation-link.interface";
import {Observable} from "rxjs/Observable";
import "rxjs/add/operator/map";

const LINKS_API = '/api/links';

@Injectable()
export class NavigationService {

    constructor(private http: Http) {
        console.log(this.http)
    }

    getNavigationLinks(): Observable<NavigationLink[]> {
        console.log("NavigationService.getNavigationLinks()");

        return this.http
            .get(LINKS_API)
            .map((response: Response) => {
            return response.json();
        })
    }
}