import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";

// containers
import {NavigationComponent} from "./containters/nagivation/navigation.component";

// components
import {NavigationLinkComponent} from "./components/navigation-link/navigation-link.component";

// services
import {NavigationService} from "./services/navigation.service";

@NgModule({
    imports: [
        CommonModule
    ],
    exports: [
        NavigationComponent
    ],
    declarations: [
        NavigationComponent,
        NavigationLinkComponent
    ],
    providers: [
        NavigationService
    ]
})
export class NavigationModule {
}