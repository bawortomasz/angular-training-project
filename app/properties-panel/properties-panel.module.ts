import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {PropertiesPanelComponent} from "./properties-panel.component";

@NgModule({
    imports: [
        CommonModule,
    ],
    declarations: [
        PropertiesPanelComponent
    ],
    exports: [
        PropertiesPanelComponent
    ]
})
export class PropertiesPanelModule {
}