import {Component} from "@angular/core";


interface Property{
    key: string,
    value: string,
    typeOfProperty?: TypeOfProperty
}

enum TypeOfProperty {
    Text, Number, Boolean
}

@Component({
    selector: 'app-properties-panel',
    styleUrls: ['./properties-panel.component.scss'],
    template: `        
        <ul *ngIf="properties.length > 0">
            <li *ngFor="let prop of properties">
               
                <div [ngSwitch]="prop.typeOfProperty">
                    <div *ngSwitchCase="typeOfProperty.Text">
                        {{prop.key}} <input [value] = "prop.value" type="text"/> 
                    </div>
                    <div *ngSwitchCase="typeOfProperty.Number">
                        {{prop.key}} <input [value] = "prop.value" type="number"/>
                    </div>
                    <div *ngSwitchCase="typeOfProperty.Boolean">
                        <input [value] = "prop.value == true ? true: false" type="checkbox"/>
                        {{prop.key}}
                    </div>
                </div> 
            </li>
            
        </ul>
        <button (click)="saving()">Save Properties</button>
    `
})
export class PropertiesPanelComponent {

    properties: Property[] = [{
        key: 'Is Good',
        value: 'true',
        typeOfProperty: TypeOfProperty.Boolean
    }, {
        key: "Number Property",
        value: "123",
        typeOfProperty: TypeOfProperty.Number
    }, {
        key: "String Properyt",
        value: "PropertyVal",
        typeOfProperty: TypeOfProperty.Text
    }];

    typeOfProperty =  TypeOfProperty;

    constructor() {

    }

    saving() {
        console.log("Saving ")
        console.log(JSON.stringify(this.properties))
    }


}