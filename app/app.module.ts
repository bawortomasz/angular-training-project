import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {CommonModule} from '@angular/common';
import {HttpModule} from "@angular/http";

import {AppComponent} from './app.component';
import {PropertiesPanelModule} from "./properties-panel/properties-panel.module";
import {NavigationModule} from "./navigation/navigation.module";

@NgModule({
    imports: [
        // angular
        BrowserModule,
        CommonModule,
        HttpModule,

        // custom
        PropertiesPanelModule,
        NavigationModule
    ],
    bootstrap: [
        AppComponent
    ],
    declarations: [
        AppComponent,
    ]
})
export class AppModule {
}
