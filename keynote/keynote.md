Angular2
======
Warsztaty

---------

1. Wprowadzenie
---------

2. Aplikacja Demo
---------

3. Angular z lotu ptaka
	- Zalety Angulara 2
	- Części składowe
	- Usługi (services)
	- Ćwiczenie 1 
---------

4. Angular CLI
 - Angular CLI
 - Kompilacja z wyprzedzeniem (AOT)
 - Instalacja Angular CLI
 - Ćwiczenie 2
---------

5. Podstawy budowania komponentów
	- Tworzenie klas komponentów (CIDER) Features Modules/Shared Modules
	- Cykl życia Preloading
	- Demonstracja użycia komponentów
---------

6. Szablony
	- Wiązania (bindings)
	- Operatory Hashtag, Gwiazdka i Elwis Manipulacja DOM Dobre praktyki
	- Ćwiczenie 4
---------

7. Serwisy
	- Definiowanie serwisu
	- Ćwiczenie 5
---------

8. Routing 
Child Routes
---------

9. Przegląd Angulara 2
 - Ćwiczenie 6
---------

10. Kompozycja komponentów
 - Architektura oparta na komponentach
 - Kontrakty 
 - Dekoratory @Input i @Output
 - Przegląd kontraktów
 - EventEmitter
 - Zadanie 7
---------

Dzień 2
====
11. Formularze
	- Wiązanie danych i komponentów
	- Formularze sterowane szablonami
	- Grupowanie i walidacja FormControls
	- Użycie styli przy walidacji
	- Mutowanie stanu
	- Ćwiczenie 8
---------

12 Komunikacja z serwerem
	- Moduł HTTP Asynchroniczna walidacja, podpowiedzi przy wyszukiwaniu
	- Observables i Nagłówki
	- Observables vs Promises
	- Ćwiczenie 9
---------

13 Reaktywność z lotu ptaka
 - dlaczego reaktywne aplikacje
 - Reaktywność w Angular 2
 - Wykrywanie zmian i asynchroniczne potoki
 - Ćwiczenie 10
---------

14 Reaktywna obsługa stanu
 - Redux
 - reducers
 - Ćwiczenie 11
---------

15 Niemutowalność
 - Przegląd reduxa
 - Niemutowalne operacje
 - Ćwiczenie 12
---------

16 Obserwowalne strumienie
 - Wprowadzenie do używania observables
 - Observables i wzorzec obserwatora
 - Ćwiczenie 13
 - Obsługa wielu strumieni
 - Ćwiczenie 14
---------

17 Reaktywne dane
 - Wprowadzenie
 - Obsługa asynchronicznych operacji
 - Obsługa wielu modeli
---------

18 Podsumowanie
---------
```
project
│   README.md
│   file001.txt    
│
└───folder1
│   │   file011.txt
│   │   file012.txt
│   │
│   └───subfolder1
│       │   file111.txt
│       │   file112.txt
│       │   ...
│   
└───folder2
    │   file021.txt
    │   file022.txt
```

